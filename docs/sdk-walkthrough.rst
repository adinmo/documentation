======================
Unity SDK Walkthrough
======================

:SDK Version: 3.0.306
:Oldest Supported version of Unity: 2020.3.41

Below you’ll find our handy SDK walkthrough guide to using the AdInMo SDK with your game project within Unity.

.. contents:: Table of Contents
    :depth: 2


Register with AdInMo
***************************

To begin using the AdInMo SDK, you need to create an account on our developer portal.

1. Go to `AdInMo Website <https://www.adinmo.com/>`_ , select the 'Portal' dropdown and click Sign Up. You can sign up using your email address or your Google account.
2. Fill in the appropriate details and confirm your account using the instructions given. If signing up via email you will be asked to confirm your account.

Creating a Game
***************************

A **Game** is a representation of your game within AdInMo.
During this process you will create a **Game Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. `Login <https://portal.adinmo.com/#/login>`_ to your developer account on AdInMo and Click the **+ Add New Game** button.
2. Fill in the details of your game and click **Save**.  These details can be edited at any time.
| - Core Game - contains all basic information about your game such as name, genre, Age Rating and store URLs. You can add multiple app stores, including alternative stores such as Samsung and Amazon.

.. image:: img/303_1.png

| - Sensitive Categories - here you can allow or restrict certain ad traffic on a per game basis

.. image:: img/303_2.png

| - Clickable Ads  - AdInMo’s InGamePlay  3.0 includes  click mechanics and the InGamePlay Magnifier. Clickable placements utilize a pre-click visualizer to encourage player interaction and reduce confusion. You can customize clickable placements, options include glowing border, colours options for the  InGamePlay  Magnifier and haptic feedback

.. image:: img/303_3.png

3. Each game has its own **game key**, this can be viewed on the details page of a game. **You will need this game key later on in the process**.
4. To view the details of your game, simply click the name of the game you want to view, either on the overview or game list page.
5. **Make sure to add your game store URL in the appropriate field within the game details page, otherwise your game will not receive any ad traffic.** 

Creating a Placement
*******************************

This part requires at least one game on the portal.

**Placements** are simply objects which will display an advertisement, they must contain a Unity 2D texture. One game can support multiple placements.

During this process you will create a **Placement Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. View the game you wish to create a placement for on the portal and click the, **+ Add New Placement** button, if you already have any placements they can be viewed here.
2. Fill out the details of your placement and select an aspect ratio. Placements can be edited at any time.
3. Each placement has its own **placement key**, this can be found on the placement list. **You will need this placement key later on in the walkthrough**.
4. AdInMo InGamePlay 3.0 enables clicks which can be enabled on a per placement basis. See Clickable Ad Units below for more information.

.. image:: img/303_4.png

**Clickable Ad Units**

The click feature is available for all ad formats (display, rich media, video, audio) and all placement types (landscape, portrait, square, TV and the audio prefab. Click functionality  can be set on a per placement basis.

AdInMo’s clickable ad units have the following key features:

1. Pre-Click Visualizer: this indicates to players an in-game ad is interactive and actionable to remove any player confusion during gameplay.

.. image:: img/303_5.png

| - You can choose from 3 different border options:
| Pulse (default setting)
| Hue Cycle
| No Cycle

2. InGamePlay Magnifier: this feature improves viewability and prevents accident clicks out of the game. If a player clicks on the ad placement the InGamePlay Magnifier pauses the gameplay and increases the size of the actual ad. This creates a better player experience improving both advertiser metrics and developer monetization & retention.

Example of a clickable ad with a purple glow border

.. image:: img/303_8.png
  :width: 360px
  :height: 733px

**AdInMo recommends:**

| The border glow is enabled by default to differentiate clickable from non-clickable placements. For the best player experience, we recommend keeping the option active. This will also highlight your Cross Promo or IAP Boost campaigns.

*IMPORTANT*

In some cases layers can obstruct the placement and prevent clicks from being registered. From the manager prefab you can select which layers to ignore specifically for the click functionality as seen in the screenshot below. This can be tested directly in editor

.. image:: img/click_ignore.png
  :width: 448px
  :height: 182px


**InGamePlay Magnifier**

| Once the player clicks the ad placement it activates the InGamePlay Magnifier which allows the player to see the ad in more detail. For video ads the player can watch the full ad with volume enabled.
| You can customize the appearance of the InGamePlay Magnifier by selecting an option from the drop menu.

.. image:: img/303_7.png

| The InGamePlay Magnifier also features an additional Call To Action which can be localized. 

Example of InGamePlay Magnifier with ‘Go To Website’ CTA:

.. image:: img/303_6.png
  :width: 360px
  :height: 733px

AdInMo’s click mechanics require the following callback functions as AdinmoManager class static calls.

.. code-block:: csharp
  :linenos:

  static public void SetPauseGameCallback(PauseGameCallback pfn);

Use to set a callback in your game triggered when a clickable ad link or the InGamePlay Magnifier is activated which enables you to take required actions  i.e.  pause the game and mute sound.

| delegate is defined as below

.. code-block:: csharp
  :linenos:

  delegate PauseGameCallback();



.. code-block:: csharp
  :linenos:

  static public void SetResumeGameCallback(ResumeGameCallback pfn);

Use to set a callback in your game triggered when a clickable ad link or the InGamePlay Magnifier is activated and enables you to take required requirements i.e.  pause the game and mute sound.

| delegate is defined as below

.. code-block:: csharp
  :linenos:
  
  delegate ResumeGameCallback();
  

**AdInMo recommends:**

| Enabling the InGamePlay Magnifier is important for IAPBoost as it does the following:
1. Clearly shows the specific IAP offer.
2. Prevents accidental clicks activating the transaction mechanic and confusing or annoying the Player.


Sandbox Mode
*******************************

AdInMo’s Sandbox Mode allows you to test your InGamePlay integration and all  the SDK features and tools. You can fine-tune your setup without affecting revenue, traffic or player experience until you are ready for live deployment.

New games are created in Sandbox Mode by default in the Developer Portal. To enable Sandbox Mode for existing games:

1. Go to your game’s Edit Page
2. Select Sandbox Mode from the game status dropdown menu

.. image:: img/sandbox_game.png

This will disable paid traffic to your game. Campaigns you have created in the Campaign Manager, e.g. CrossPromo or IAPBoost will still work. 

If you don’t want to pause the entire game but want to test new features, you can enable Sandbox Mode for a single device.

1. Select the game you wish to add a test device for
2. Select Test Devices and complete the required info
3. Click ‘Find’

.. image:: img/add_test_device.png

You will see a list of devices based on your criteria that have played the game in the last six hours. It may take a couple of minutes for the devices to appear. 

You can then choose to enable Sandbox Mode or simply enable device logs for debugging.

.. image:: img/test_device_edit.png

**Please note:**

- Once testing is complete, you must switch your game status to **LIVE** to enable traffic and start generating revenue. Failure to do so will prevent your integration from functioning properly.

- If you create a new instance of your project your AdInMo ID may change even if you are using the same device and you will need to add the device to Test Devices again


Setting up the SDK within Unity
*********************************

After you’ve set up the account, you can then download our latest SDK `here <https://adinmo-website.s3.eu-west-1.amazonaws.com/SDK/adinmo_v3.0.306.unitypackage>`_

Oldest Supported version of Unity 2020.3.41

Release notes for the AdInMo SDK can be found `here <https://documentation.adinmo.com/en/latest/release-note.html>`_

**When updating an older version of the AdInMo SDK, the best practice is to delete the existing AdInMo folder in assets before importing the latest version of the SDK.**

1. After you’ve downloaded the SDK, create or open a project within Unity Editor. If you’ve downloading from the Unity Asset Store, you can either import from the package manager or through the asset store tab depending on the unity version.
2. From the top menu, click Assets > Import Package >  Custom Package and locate the Unity package containing the SDK. Ensure all the items are selected and click **Import**.
3. You should now see a folder called Adinmo displayed in the Unity Assets window.

AdInMo Manager
*********************************

1. First, the AdinmoManager prefab needs to be added to the scene. Next, copy and paste the game key of your game into the AdinmoManager. 

2. From the Assets window, select Adinmo > Prefabs and click and drag the prefab called Adinmo Manager into the scene hierarchy.

3. Navigate back to the portal and to the appropriate game, view the details of the game on the portal and copy the game key (starts with gk…). In Unity Editor, click on the AdinmoManager prefab and paste the copied game key in the game key field.

4. You can report your application version through the Adinmo Manager inspector by adding it in the Application Version field. 

*NOTE:* Make sure adinmomanager game object is initialized on game Start to avoid any initialization errors.


Adding placements
*********************************

AdInMo SDK 2.0 supports game objects templates (image, quad and sprites) of different aspect ratios that you can add to your scene without the need to manually create them.

1. Go to the AdInMo folder in the assets window and select prefabs. 
2. From here, you can select the appropriate prefab folder for the game object you want to add. Each game object is named with its aspect ratio.
3. Paste the placement key of the placement from the portal into the placement key field. Placement keys start with 'pk…'
4. Test your project directly in editor by hitting Play. You can enable AdInMo's Debug tool from Adinmo>Manager which will give you important information about your placement setting.
5. To set Audio ads:

| - Drag and drop the Audio prefab to an existing or a new canvas.
| - You will now see the Audio icon present in your scene. The prefab is disabled by default and the icon will not be visible during runtime until an ad is ready to play.
| - From within the prefab you can move the icon to where you want it to show in-game. It will remain hidden until an ad is ready to play. The icon is set to scale with the reference resolution on the canvas and by default is set to 100x100 pixels.

*NOTE*
The icon should be placed in an area of the game that is visible to the user but not disruptive to the game experience.

6. To set Rewarded InGamePlay

| - Create a canvas or use an existing one. Icon and popup will scale with Reference Resolution. We suggest 1920x1080 for portrait or 1080x1920 for landscape,
| - Drag and drop the ZebedeePrefab onto the canvas - you can choose Portrait or Landscape based on your setup. 

| Make sure to set the canvas to match width for portrait or height for landscape. From within the prefab you can decide whether to put the icon left or right side on your screen based on your preference.

| Your scene should look like this:
.. image:: img/Screenshot_20240207-173537.png

| During runtime, when clicking on the icon you will be presented with this popup:

.. image:: img/image__53_.png

| By default the icon, popup and success screen are in green however you can adjust the colour to suit your game.

SDK 2.5.253 simplified how ad placement sizes are managed with four Placement Types supported:

- Square (min 250 x 250 pixels)
- TV (min 480 x 320 pixels)
- Portrait (min 320 x 480 pixels)
- Landscape (min 300 x 60 pixels)

The four Ad Placement Types map to IAB’s Fixed Size Ad Specifications and support all display and video ad units for maximum demand opportunities. 
Aspect ratios that do not match these Placement Types are deprecated and no longer be added to your game. Existing ad placements will continue to serve ads but will generate less revenue and we recommend editing all placements to use one of the four Ad Placement Types.

Make sure to set your texture quality High. You can set it by going to Project Settings>Quality>Texture Settings. This will allow you to scale the placement if your project requires it and still have a high quality ad. This will not affect your app’s performance.


CrossPromo
************************
CrossPromo campaigns can be used to promote other games from your portfolio. By using a tracking URL you can monitor the ad click rate on the portal

.. image:: img/303_9.png

| To set up you need to:

1. Choose your images or videos following the supported aspect ratios and file size. For the best player experience, we recommend images to be approx. 400kb in file size and that video files do not exceed 700kbps bitrate.
2. Set click target URLs
3. Complete the campaign details including targeting
4. Make sure your placements are enabled for click. See more on setting up `clickable <https://documentation.adinmo.com/en/latest/sdk-walkthrough.html#clickable-ad-units>`_
5. When you're ready for the campaign to go live please drop us a quick email to support@adinmo.com. We will then do a quick brand safety and copyright check on the creatives to make sure they meet clauses 3 and 4 of our `terms <https://www.adinmo.com/advertiserterms/>`_
6. Activate! You can set your campaign live by changing the status in the Campaign Manager

IAPBoost
************************
With IAPBoost campaigns you can promote your IAP offers in-game for a frictionless player experience.

| You can utilize any InGamePlay ad format (display, rich media, or video) and your creatives for consumables etc will appear in the standard ad placements during gameplay. When players click on an ad, the game pauses, and the InGamePlay Magnifier highlights the IAP offer. Players can then choose to proceed with the IAP purchase in the usual way or close the InGamePlay Magnifier to resume gameplay.
| Once the purchase is finished, gameplay is unpaused.

*IAPBoost Campaign Setup*

To set up your IAPBoost campaign using the Campaign Manager select the IAP item/pack you want to promote within your game and set its IAP id (Product ID in Google Play, Apple Store and Samsung Store, SKU in Amazon store) to reference the campaign in your code. You can run multiple campaigns at the same time and create dependencies between them to optimize your IAP conversion strategy

| - Set general campaign details such as the campaign name, purchase caps and limits
.. image:: img/303_10.png

| - Set campaign dependencies which will send your campaign to players if certain conditions are met. (For example, if a player has not made a purchase of campaign A, they will not receive an ad for campaign B)
.. image:: img/303_11.png

| - Set your campaign flight period, targeting and tracking
.. image:: img/303_12.png

| - Last but not least, add your appropriate creatives
.. image:: img/303_13.png

*IAP Purchase Flow*

1. Your IAPBoost ad is displayed to the player using standard InGamePlay ad placements. We recommend using the Pre-Click Visualizer options to let your players know that the placement is clickable and the offer is actionable. (see below).

.. image:: img/303_14.jpg
  :width: 360px
  :height: 733px

2. The player clicks on the ad placement and the InGamePlay Magnifier highlights the IAP offer. A callback takes place and the game is paused.

| The player can click on the offer to proceed to purchase or can exit the offer by clicking anywhere else on the screen or on the close button and gameplay is resumed.
.. image:: img/303_15.jpg
  :width: 360px
  :height: 733px

3. If a player wishes to proceed, the next click activates the platform-specific IAP transaction mechanism and via the callbacks the player can seamlessly complete the purchase within the game removing the need to wait for a break in play to complete the IAP purchase.

.. image:: img/303_16.jpg
  :width: 360px
  :height: 733px

4. Once the payment transaction is completed, the IAP is credited to the player and is immediately available to use. The player returns frictionlessly to the gameplay and resumes gameplay.

*Creative Assets*

Creative assets for IAPBoost campaigns can be uploaded via the developer portal. They will be served into your games at the most appropriate times based on the server logic described below.

*Serving Logic*

IAPBoost ads can be served when no paid ad is available. Machine learning will determine the right time to show an IAPBoost ad to a player based upon previous campaign performance.

| For example, the AdInMo serving logic will:

1. Exclude serving the offer again if the maximum number of all time purchases have been made.
2. Exclude serving the offer again for that day if the maximum number of daily purchases have been made.
3. Allow a cool down period before serving the offer again if it is a repeat purchase offer.

*Single Use vs Repeat IAPs*

Daily caps and all-time caps on IAPs are supported as well as an ‘IAP cooldown’ period. Our serving logic will adhere to the rules you set around caps and cooldown periods. **If no rules are set, after purchase the campaign will have a 1 hour cooldown after which the ad may be served again.**

*Tracking*

| Campaign performance data is available via the developer portal.

| Best practice would be to allow for all your iap_ids to be tracked via our callbacks. This way our algorithms will be able to more closely monitor the purchase probability for each individual player  and choose the right time to show them an offer.

*Setting up IAPBoost callbacks*

| Implement the following callbacks when enabling IAPBoost Campaigns


.. code-block:: csharp
  :linenos:

  static public void SetInAppPurchaseCallback(InAppPurchaseCallback pfn);

This callback is used to tell the game that the player has requested to purchase an IAP using AdInMo’s IAPBoost interface and the developer is required to handle the IAP process and call InAppPurchaseSuccess or InAppPurchaseFailed after the process has completed.

delegate is defined as below, where iap_id is the store_id of the IAP delegate

.. code-block:: csharp
  :linenos:

  InAppPurchaseCallback(string iap_id);


.. code-block:: csharp
  :linenos:

  public static void AdinmoManager.InAppPurchaseSuccess(string id, string isoCurrencyCode, float localizedPrice, string transactionID);

Call this to indicate to the SDK that a successful in app purchase has been made, where id is the store_id of the IAP, isoCurrencyCode is the currency code eg USD, localisedPrice is the price of the IAP in local currency, and transactionID is the transactionID of the purchase, (for anti fraud purposes)



.. code-block:: csharp
  :linenos:
  
  public static void AdinmoManager.InAppPurchaseFailed(string id, string isoCurrencyCode, float localizedPrice, string failureReason);
  
Call this to indicate to the SDK that a failed attempt has been made to make an In-app Purchase, where id is the store_id of the IAP, isoCurrencyCode is the currency code eg USD, localisedPrice is the price of the IAP in local currency, and failureReason is a reason why the purchase failed. Where possible please use standard Google or Apple reason codes.


Here is an example of the above callbacks in a script:



.. code-block:: csharp
  :linenos:

  using UnityEngine;

  public class IAPBoostHandler : MonoBehaviour
  {
     void Start()
     {
         // Set the callback for IAP requests
         AdinmoManager.SetInAppPurchaseCallback(OnInAppPurchaseRequested);
     }

     // Callback function for handling IAP requests
     private void OnInAppPurchaseRequested(string iap_id)
     {
         Debug.Log($"IAP Requested: {iap_id}");
        
         // Simulate purchase process (replace with your IAP library logic)
         bool purchaseSuccessful = SimulatePurchase(iap_id);

         if (purchaseSuccessful)
         {
            // Notify AdInMo of a successful purchase
            AdinmoManager.InAppPurchaseSuccess(
                iap_id,             // Store ID of the IAP
                "USD",              // Example currency code
                4.99f,              // Example localized price
                "TRANSACTION12345"  // Example transaction ID
            );
         }
         else
         {
             // Notify AdInMo of a failed purchase
             AdinmoManager.InAppPurchaseFailed(
                iap_id,             // Store ID of the IAP
                "USD",              // Example currency code
                4.99f,              // Example localized price
                "Payment declined"  // Example failure reason
             );
         }
     }

     // Simulated purchase logic for demonstration purposes
     private bool SimulatePurchase(string iap_id)
     {
         // Replace this with your actual IAP processing logic
         Debug.Log($"Processing purchase for IAP ID: {iap_id}");
         return Random.value > 0.5f; // Randomly simulate success or failure
     }
  }



*NOTE*

You can add the callbacks to any script you wish or create a separate script solely for the IAPBoost callbacks and attach it to an empty object.


AdinmoTexture Component  
*********************************

If you want to create your own game object from scratch, you can use the AdInMo texture component on the game object that will act as the advertising placement within your game.

1. Create a game object, either an image, quad or sprite, select the game object in the scene and click Add Component in the inspector window.

2. In the search box, type and select Adinmo Texture.

3. Paste the placement key of the placement from the portal into the Adinmo Texture component, Placement keys start with pk…

*NOTE*
Keep in mind this method is prone to errors and it’s always best to use the prefabs.


App-ads.txt
*********************************

| App.ads.txt is an industry mechanism developed by IAB to help combat ad fraud. Demand partners check your app-ads.txt file to verify that bid requests are coming from authorized sellers. 
| It is essential to keep your app-ads.txt file updated to maximize monetization.

| Existing app-ads.txt file:
| If you already have an app-ads.txt file on your website, you just need to add AdInMo’s details which you can download from the developer portal main page.

.. image:: img/app-ads.png

Non-existing app-ads.txt file:

1. Download the latest app-ads.txt file from your portal page.
2. Ensure your web server can serve your developer url as plain text.
3. Place the app-ads.txt file provided in the root directory of your website. 
4. Add the URL containing your app-ads.txt file on your Company profile in the portal. This step is crucial so we can verify it.

|  The URL should look something like this: www.developer_name.com/app-ads.txt

*NOTE*

|  In case you don’t have a website yet or your hosting company doesn’t give you access, you can use a third party service to host your app-ads.txt file. 


.. image:: img/app-ads1.png

*NOTE*

| The root directory of your website or the web root is the folder where the index.html file is stored and it is publicly accessible. 
| Anyone typing your URL followed by /app-ads.txt should be able to view your app-ads.txt file. 

User Consent GDPR
******************

| From January 16, 2024, Google requires publishers and developers to use a Consent Management Platform (CMP) that has been certified by Google and has integrated with the IAB's `Transparency and Consent Framework <https://iabeurope.eu/transparency-consent-framework/>`_ (TCF) when serving ads to users in the European Economic Area or the UK. 

| Using a CMP enables you to pass the required TCF2.2 to monetize your traffic in Europe. 
| AdInMo’s SDK supports any CMP. 

| Please see further information below on how to pass the TCF2.2 string using:
| AdMob UMP 
| Usercentrics


Accessing the advertising ID in Android API33 and above
#######################################################

In version 33 of the Android SDK it is no longer possible to access the advertising id without adding an additional permission to the Android Manifest. In this case there will be an warning in logcat like this:-
Package <your app_id> failed Ad Id permission check. Apps that target Android SDK 33 or higher should declare com.google.android.gms.permission.AD_ID in the app manifest to access Ad Id.
And result in AdInMo not being sent the correct advertising id during the bidding process, which could result in reduced income for Android devices.
Of course, you could fix this by compiling against an earlier version of the Android SDK, but this is not recommended. The preferred fix is to add the missing permission to the Android Manifest

*Adding Missing Permission*


| 1.If you already have a custom AndroidManifest.xml file continue to step 6, otherwise continue

| 2.Go to File/Build Settings

| 3.Select Android

| 4.Select Player Settings

| 5.Find the Custom Main Manifest option in Publishing Settings/Build and tick the box. Unity will create a file and tell you where it is:

.. image:: img/API33_1.png

| 6.Edit the file indicated and add: 

.. code-block:: csharp
  :linenos:

  <uses-permission android:name="com.google.android.gms.permission.AD_ID"/> 

tag just above the closing tag '</manifest>' as indicated below:

.. image:: img/API33_3.png

| or an alternative option would be:

*Downgrading Android SDK*

If it is preferable instead to downgrade the SDK version used instead. Here is what you would do:

| 7.Go to File/Build Settings

| 8.Select Android

| 9.Select Player Settings

| 10.Find Target API Level in the Other Settings section

| 11.Change to an earlier version than API level 33

.. image:: img/API33_2.png

In both cases the warning should no longer appear in logcat, and AdInMo will now receive the correct advertising id.


Using AdMob’s UMP plugin to validate GDPR consent
#################################################



*Setup Admob*

These instructions only work if you have a working Admob account via https://apps.admob.com/ for the game that you want to use. It may be possible to create an Admob account and register your game without actually having integrated Admob into your project, but at the bear minimum this will require your game to be live on the app store.
Note that you may need to temporarily disable any adblocker before using the site.
Once you have a working Admob account, use the following instructions to setup GDPR verification.

1. From your Apps. Find your game, and make a note of the App ID (starts with ca-app-pub-), you will need this further down the process where it’s mentioned as Admob App Id

.. image:: img/Picture1.png

2. From Privacy & messaging click GDPR

.. image:: img/Picture2.png

3. Click Continue

.. image:: img/Picture3.png

4. Click “Create a GDPR message”

.. image:: img/Picture4.png

5. Click Select Apps

.. image:: img/Picture5.png

6. Select your app that you want to apply this too, you will need to set a privacy policy URL, and confirm
 
7. Set up you language preferences

.. image:: img/Picture6.png

8. Setup your user consent options, you will need the first option, ‘Consent’ or  ‘Manage options’, and the check the Close (do not consent)

.. image:: img/Picture7.png

9. Targeting should be for Countries subject to GDPR

.. image:: img/Picture8.png

10. In the Review your account settings section tick both boxes

.. image:: img/Picture9.png

11. Click Review your ad partners

12. Find Adinmo Ltd in the list and add it

.. image:: img/Picture10.png

13. Click Confirm
14. Click Add purposes
15. Configure the usages as per your own preferences, but we recommend the following settings will maximise potential earnings

.. image:: img/Picture11.png

16. Press confirm
17. Press continue and you should be presented with a preview of what your consent dialog will look like
18. Give your message a name and press publish

*Android Specific Setup Instructions*

1. Get Admob Application id for your android app and add it to AdinmoManager Object like this:

.. image:: img/Picture12.png

2. Add the required dependencies for UMP to an Android build:

* Install External Dependency Manager
* After installing the unitypackage, it will ask you if you want to auto resolve, if you click 'Yes', that is all you need to do
* If you click 'No', you can do the resolve by using Assets > External Dependency Manager > Android Resolver > resolve
* After a while when it copies all the missing dependencies to the Plugins/Android folder, it will complete and you can run the build


*iOS Specific Setup Instructions*

1. Get Admob Application id for your iOS app and add it to AdinmoManager Object like this:

.. image:: img/Picture13.png

2. Add the required dependencies for UMP to an iOS build:

* Make sure you have CocoaPods installed on your development machine
* Install External Dependency Manager
* Build your app as usual
* Note, this setup will not support bitcode

*Using UMP*

After AdinmoManager has been initiated, there are 2 functions you can use to trigger UMP events:


.. code-block:: csharp
  :linenos:
  
  static void AdinmoManager.CheckConsent([Optional]UMPCallback resultCallback)


Call this at a convenient time during startup. If consent is required, and hasn’t been determined yet, this will launch a popup asking for consent options, otherwise it will notify adinmo about previously selected options saved in the prefs. 


.. code-block:: csharp
  :linenos:
  
  static void AdinmoManager.ResetConsent([Optional]UMPCallback resultCallback)


This will reset the consent received state and bring up a fresh popup. This is designed to be used in the game menus to allow the user to change their mind about their previously selected consent options

Where UMPCallback is an optional delegate of the format:


.. code-block:: csharp
  :linenos:
  
  void UMPCallback(bool consentWasRequired, bool canShowAds, bool canShowPersonalisedAds, string TCF_String);

You can use it to receive the results of the dialog box for your own purposes.

Using UserCentrics
##################
Guides on how to setup Usercentrics can be found here `Usercentrics Documentation <https://docs.usercentrics.com/?_gl=1*270569*_ga*MTczMTk0NTIzNS4xNjk1MTE3NTU5*_ga_2QB41H1S1Q*MTY5NTExNzU1OC4xLjEuMTY5NTExNzYxMS43LjAuMA..#/>`_ and a guide to setting up their SDK can be found here Introduction - `Usercentrics Apps SDK <https://docs.usercentrics.com/cmp_in_app_sdk/latest/unity/introduction/>`_.
While setting up Usercentrics, ensure that AdInMo Ltd is enabled as a vendor


*NOTE*

| Make sure to add 'using Adinmo;' at the beginning of your script so you can use any of the methods.

| You also need to call the consent methods **after** the AdinmoManager is created. Depending on your setup it should look something like this:

| 1.create AdinmoManager

| 2.do consent popup


Finished!
***************************

Run your game and advertisements will appear in your game.

For support, please check our `FAQ <https://www.adinmo.com/developer-faq-new/>`_.

Please Note!
############

**You will not be able to see impressions using a development build, uncheck this option if you are building a release.**

Unity Editor also has its own development build option under **File**, **Build Settings**. Ensure the **Development Build** option is not checked when building for release.

