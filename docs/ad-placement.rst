====================================
Ad Placements and Debugging Tools
====================================

This section covers other features that are available with placements. While not a requirement for the AdInMo SDK, we highly recommend you make use of these features.

Impression Validity
********************

To maximize your monetization it’s essential to understand what makes an impression valid. Here is a summary of what makes an impression valid:

* Time on screen: 
|  - Rich media - 1 second + 4 second cooldown - with cooldown being the minimum amount of time between cycling (ad switching). Through the portal you can increase the time an impression cycles before the next ad, from minimum of 4 to a maximum of 30 seconds. Slower cycles may impact monetization.
|  - Video ads - 2 seconds of screen time and do not have a cooldown period. The ad will be played in it's entirety. Depending on demand, this may vary between 6 to 30 seconds in length.
* Angle must be less than or equal to 55 degrees, the image must also be facing the camera.
* Placement size must be at least 1.5% (coverage).
* Occlusion - this relates to blocking the placement e.g. putting a game object in front of placement blocking it from the camera. 
| - Audio ads - will play a short 15 or 30 second audio ad to generate an impression. Device audio must be at 30% volume or higher for the ad to play. Audio ads have a cool off period of 5 minutes.


**NOTES:**

We recommend using the debugging tool to help optimise your placements to ensure they are generating valid impressions. For more information on the debugging tool see below.

We recommend always setting your ad placements in places players are most likely to spend time.  The SDK now tracks Dwell Time as an attention metric: the time a player spends looking at a valid placement. Optimizing for Dwell Time will drive higher CPMs.


Debugging Tools
****************
AdInMo’s debugging tools will help you optimise your placements.

1. Within Unity Editor go to the top menu, Adinmo > Manager. A new window will open with the debugging tools. Here you can also view your game key, placements and more. 
2. Enable ‘Basic debug’ to view impression validity and failed reasons.
3. Enable 'Advanced debug' to get more detailed information for your placement such as the size and angle of the placement relative to the screen, as well as the name of the placement, the aspect ratio and the camera 'looking' at it.

Either option can be selected both from the popup window or from Adinmo Manager prefab inspector. If you would debug on your device, make sure to select 'Debug on device' as well.

Validity will be determined by the green strip on top of the placement. When it turns from green to white will mean the placement has met all criteria and has been on screen for one second - a valid impression has been registered.


.. image:: img/debug.jpg


**Occlusion Test Ignores Layers**

Note this feature is only available in **Unity SDK 2.0.3 and above**.
This feature allows you to set specific layers to be ignored by the occlusion test. An example of where this might be used is when a placement has a collision box that may result in impressions being lost. This option can be found on the **adinmo texture component**.

.. image:: img/occlusion.jpg



**Fail Reason**

| **Too Small** - placement is failing the Placement Size check. Placement is less than 1.5% relative to the screen.
| **Angle to Camera** - placement is failing the Angle check, angle is larger than 55 degrees.
| **Not Facing Camera** - Placement is failing the angle check and not facing the camera
| **Not in Shot** - Placement is not on screen. Check if the correct camera has been assigned.
| **Occluded** - an object occludes the placement causing less than 50% of the placement to be visible. This applies to collision boxes as well.
| **Partially off-screen** - Less than 50% of the placement is visible relative to the active camera.


Placeholder Image
********************

On the AdinmoTexture component there is an option to disable the default placeholder. By default there is an AdInMo placeholder, this will display when no ads are being displayed. You can have your own placeholder image here by checking the disable placeholder option and adding your own to the texture.

.. image:: img/placeholder.jpg


Update Adinmo Placements to work with URP
******************************************

When using a placement prefab in a URP scene, you will notice that the texture doesn’t render properly, and it will appear magenta, this is because the default shader isn’t URP compatible.

.. image:: img/pink_URP.png

To solve this problem, you will need to replace the Default-Material with a URP compatible material:

1. Right click in your project window and select create/material

.. image:: img/menu_URP.png

2. Rename it to something appropriate

3. Select the material, click on the shader box and select Universal Render Pipeline, then an appropriate shader, usually lit or unlit

.. image:: img/material_URP.png

4. Then select your prefab, expand the material list on the renderer

.. image:: img/mesh_URP.png

5. Find your material in the popup and select it

.. image:: img/material_URP2.png

6. The placement should change to non magenta, indicating that it is working

.. image:: img/nonmag_URP.png


Background Setting
**********************
You can set the space around your placement through the portal to better control how it looks in-game. You can choose to do this by colour-matching or shrink the placement to fit the ad.


Android Player Settings
**********************

In Unity 2019 and above, you may find that Vulkan is selected by default. The SDK will support Vulkan as of version 2.1.1 and above with recommended Android version 9.0 and above. 

.. image:: img/vulkan.png


Dynamic Ads
************

AdInMo supports the concept of dynamic ads, these are ad placements that are created programmatically rather than through the Unity editor. This allows AdInMo to support situations where a game developer programmatically creates objects in the game, our SDK supports the concept of then creating the ad unit programmatically once the object has been created.
Here’s a simple example of how this works using the AdInMo Unity SDK.

.. code-block:: csharp
  :linenos:

  using UnityEngine;
  public class InstantiationExample : MonoBehaviour
  {
    // Reference to the Prefab. Drag a Prefab with an AdInMo Texture component attached to it into this field in the Inspector.
      
    public GameObject myAdinmoPrefab;
    // This script will simply instantiate the Prefab when the game starts.
      
    void Start()
    {
        // Instantiate at position (0, 0, 0) and zero rotation.
        Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);
    }
  }



Audio Ads
********************

| The SDK will detect the audio levels and if the master volume of the device is at 30% or more, an audio ad will play. Audio ads have a 5 minute cool off period.


| Audio ads are measured using Listen Through Rates (LTR). Make sure to set the audio prefab in scenes where users are more likely to spend at least 30 seconds to get the best LTR and monetization. 


| To set Audio ads you need to:

* Add the AdInMo manager prefab to your hierarchy
* Add a canvas (you can use any existing canvas) and set a reference resolution. Preferably 1920x1080
* Drag and drop the Audio prefab from the Adinmo>prefabs folder onto the canvas
* The prefab is disabled by default, you can enable it to set the icon where you want it to be, just remember to disable it again. It will automatically enable itself when an ad comes through.

| Audio ads are set to work with Unity Default Audio as well as third party audio solutions such as FMOD. By default, the audio ad will play on full volume and will lower all other sounds when using Unity default audio until the ad ends. If you’re using FMOD or other third party audio solution, you can use these exposed methods to catch when the audio ad starts and ends and set your volume levels accordingly


.. code-block:: csharp
  :linenos:


  static public void SetAudioAdStartedCallback(AudioPlaybackCallback callback)
  
  static public void SetAudioAdFinishedCallback(AudioPlaybackCallback callback)
  
  public delegate void AudioPlaybackCallback()




| this would be an example you can put in the class :


.. code-block:: csharp
  :linenos:

  AdinmoManager.SetAudioAdStartedCallback(AudioAdStartedPlaying);

        AdinmoManager.SetAudioAdFinishedCallback(AudioAdFinishedPlaying);
    public void AudioAdStartedPlaying()
    {
        Debug.Log("Audio Ad started playing");
    }
    public void AudioAdFinishedPlaying()
    {
        Debug.Log("Audio Ad finished playing");
    }



